package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    //Variables
    private double actualNumber, saveNumber;
    private String tipoOperacion;
    private TextView tvResultado, tvResultadoguardado;
    private Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9,
    btnResta, btnSuma, btnMulti, btnDiv, btnResult, btnC, btnCE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Damos valores a las variables que usaremos
        this.actualNumber = 0.0;
        this.saveNumber = 0.0;
        this.tipoOperacion = "";

        //Bindeamos TextBox
        this.tvResultado = findViewById(R.id.tvActual);
        this.tvResultadoguardado = findViewById(R.id.tvNumguardado);

        //Reseteamos los valores
        this.tvResultado.setText(String.valueOf(this.actualNumber));
        this.tvResultadoguardado.setText(String.valueOf(this.saveNumber));

        //Bindeamos botones
        this.btn0 = findViewById(R.id.btn0);
        this.btn1 = findViewById(R.id.btn1);
        this.btn2 = findViewById(R.id.btn2);
        this.btn3 = findViewById(R.id.btn3);
        this.btn4 = findViewById(R.id.btn4);
        this.btn5 = findViewById(R.id.btn5);
        this.btn6 = findViewById(R.id.btn6);
        this.btn7 = findViewById(R.id.btn7);
        this.btn8 = findViewById(R.id.btn8);
        this.btn9 = findViewById(R.id.btn9);

        this.btnC = findViewById(R.id.btnResetAll);
        this.btnCE = findViewById(R.id.btnResetResult);
        this.btnSuma = findViewById(R.id.btnsuma);
        this.btnResta = findViewById(R.id.btnResta);
        this.btnMulti = findViewById(R.id.btnMultiplicar);
        this.btnDiv = findViewById(R.id.btnDiv);
        this.btnResult = findViewById(R.id.btnResultado);

        //=============================================
        //      EVENTOS
        //=============================================

        //-----------------------------------
        //BOTONES DE RESETEO DE VALORES
        //-----------------------------------

        //Botón 'C' resetea todas las operaciones
        this.btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualNumber = 0.0;
                saveNumber = 0.0;

                tvResultado.setText(String.valueOf(actualNumber));
                tvResultadoguardado.setText(String.valueOf(saveNumber));
            }
        });


        //Botón 'CE' resetea la última operación/Entrada de números
        this.btnCE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actualNumber = 0.0;
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });

        //-----------------------------------
        //BOTONES DE INSERCCIÓN DE NÚMEROS
        //-----------------------------------
        //Btn-0
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber *= 10;
                }
                tvResultado.setText(String.valueOf(actualNumber));            }
        });

        //Btn-1
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 1;
                }else{
                    actualNumber = 1;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-2
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 2;
                }else{
                    actualNumber = 2;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-3
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 3;
                }else{
                    actualNumber = 3;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-4
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 4;
                }else{
                    actualNumber = 4;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-5
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 5;
                }else{
                    actualNumber = 5;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-6
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 6;
                }else{
                    actualNumber = 6;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });


        //Btn-7
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 7;
                }else{
                    actualNumber = 7;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });

        //Btn-8
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(actualNumber != 0){
                    actualNumber = (actualNumber * 10) + 8;
                }else{
                    actualNumber = 8;
                }
                tvResultado.setText(String.valueOf(actualNumber));
            }
        });

        //Btn-9
        btn9.setOnClickListener(v -> {
            if(actualNumber != 0){
                actualNumber = (actualNumber * 10) + 9;
            }else{
                actualNumber = 9;
            }
            tvResultado.setText(String.valueOf(actualNumber));
        });


        //---------------------------------
        //BOTONES DE OPERACIONES
        //---------------------------------
        //Botón Suma
        this.btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvResultadoguardado.getText().toString().equalsIgnoreCase("0.0")){
                    saveNumber = actualNumber;
                    actualNumber = 0.0;

                    tvResultadoguardado.setText(String.valueOf(saveNumber));
                    tvResultado.setText(String.valueOf(actualNumber));
                }
                tipoOperacion = "suma";

            }
        });


        //Botón Restar
        this.btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvResultadoguardado.getText().toString().equalsIgnoreCase("0.0")){
                    saveNumber = actualNumber;
                    actualNumber = 0.0;

                    tvResultadoguardado.setText(String.valueOf(saveNumber));
                    tvResultado.setText(String.valueOf(actualNumber));
                }

                tipoOperacion = "resta";
            }
        });


        //Botón Multiplicar
        this.btnMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvResultadoguardado.getText().toString().equalsIgnoreCase("0.0")){
                    saveNumber = actualNumber;
                    actualNumber = 0.0;

                    tvResultadoguardado.setText(String.valueOf(saveNumber));
                    tvResultado.setText(String.valueOf(actualNumber));
                }

                tipoOperacion = "multiplicacion";
            }
        });


        //Botón Dividir
        this.btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvResultadoguardado.getText().toString().equalsIgnoreCase("0.0")){
                    saveNumber = actualNumber;
                    actualNumber = 0.0;

                    tvResultadoguardado.setText(String.valueOf(saveNumber));
                    tvResultado.setText(String.valueOf(actualNumber));
                }

                tipoOperacion = "division";
            }
        });


        //---------------------------------
        //BOTONES DE RESULTADO
        //---------------------------------
        this.btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double resultado = 0.0;
                switch (tipoOperacion){

                    case "suma":
                        resultado = Double.parseDouble(tvResultadoguardado.getText().toString()) + Double.parseDouble(tvResultado.getText().toString());
                        actualNumber = 0.0;
                        break;

                    case "resta":
                        resultado = Double.parseDouble(tvResultadoguardado.getText().toString()) - Double.parseDouble(tvResultado.getText().toString());
                        actualNumber = 0.0;
                        break;

                    case "multiplicacion":
                        resultado = Double.parseDouble(tvResultadoguardado.getText().toString()) * Double.parseDouble(tvResultado.getText().toString());
                        actualNumber = 0.0;
                        break;

                    case "division":
                        resultado = Double.parseDouble(tvResultadoguardado.getText().toString()) / Double.parseDouble(tvResultado.getText().toString());
                        actualNumber = 0.0;
                        break;

                    default:
                        saveNumber = actualNumber;
                        actualNumber = 0.0;
                        break;
                }
                tvResultadoguardado.setText(String.valueOf(saveNumber));
                tvResultado.setText(String.valueOf(actualNumber));
                tipoOperacion = "";
            }
        });
    }
}